#!/usr/bin/env python3

from os import listdir
from os.path import isdir
from shutil import rmtree
import pytest
from ncbi_downloader import *


api_key="76e89a45c3498186650efcfc11609d6d9208"
genes_num=13


class TestNCBIDownloader():
    def test_wo_multiprocessing(self):
        original_species = ["Chloebia gouldiae", "Montifringilla adamsi", "Montifringilla henrici", "Montifringilla nivalis", "Onychostruthus taczanowskii", "Padda oryzivora", "Passer ammodendri", "Passer domesticus", "Passer montanus", "Prunella fulvescens", "Prunella himalayana", "Prunella montanella", "Prunella rubeculoides", "Prunella strophiata", "Pyrgilauda blanfordi", "Pyrgilauda davidiana", "Pyrgilauda ruficollis"]
        original_species.sort()
        original_species = [original_specie.replace(" ", "_") for original_specie in original_species]
        
        for specie in main("Passer domesticus", "family"):
            pass
        species = listdir("Passer_domesticus/genes")
        species.sort()
        for specie in species:
            temp_file = f"tmp_{specie}"
            if isdir(temp_file):
                rmtree(temp_file)
        assert len(original_species) == len(species)
        for original_specie, specie in zip(original_species, species):
            assert original_specie == specie
        rmtree("Passer_domesticus/")

    def test_P_domesticus(self):
        original_species = ["Chloebia gouldiae", "Montifringilla adamsi", "Montifringilla henrici", "Montifringilla nivalis", "Onychostruthus taczanowskii", "Padda oryzivora", "Passer ammodendri", "Passer domesticus", "Passer montanus", "Prunella fulvescens", "Prunella himalayana", "Prunella montanella", "Prunella rubeculoides", "Prunella strophiata", "Pyrgilauda blanfordi", "Pyrgilauda davidiana", "Pyrgilauda ruficollis"]
        original_species.sort()
        original_species = [original_specie.replace(" ", "_") for original_specie in original_species]
        

        for specie in main("Passer domesticus", "family", api_key):
            pass
    
        path = "Passer_domesticus/genes/"
        species = listdir(path)
        species.sort()

        # delete temp files
        for specie in species:
            temp_file = f"tmp_{specie}"
            if isdir(temp_file):
                rmtree(temp_file)
        
        # assert species names
        for original_specie, specie in zip(original_species, species):
            assert original_specie == specie
        
        # assert num of species
        assert len(original_species) == len(species)

        # assert num of genes for species
        for specie in species:
            assert genes_num == len(listdir(path+specie))

        # Passer domesticus
        nd1_pd="""atgac caactacccc
     4021 ctattaatca atttaatcat aaccctctcc tacatcctcc caatcttaat cgcagtagcc
     4081 ttcctcacac tcgtagaacg caaaatccta agctatatgc aaggacgaaa aggaccaaac
     4141 gtggtaggcc catttggact tctacagccc ttagccgacg gagtgaaact attcatcaaa
     4201 gagcccattc gaccatcaac gtcttcccca atcatattcc tcacaactcc tatactagcc
     4261 ctgctcctgg cactctccat ttgaattcca ctccccctac cattttccct agcagaccta
     4321 aacctgggcc tactattcct cctagctata tcaagtctag cagtttactc catcctatgg
     4381 tcaggatgag cttctaactc aaaatacgcc ctcattggag cgctgcgagc agtagcccag
     4441 acaatctcct atgaggtcac cctggcaatc atcctactat ctgtcattct tctaagcgga
     4501 aactataccc tgggagctct agcagtcact caagaacccc tttgactcat tttctcttgc
     4561 tgacccctcg ctataatgtg gtacgtatcc acactcgccg aaactaaccg tgcccccttc
     4621 gacctgacag aaggagaatc ggaactagtc tccggattca acgtagagta ctcagcagga
     4681 ccattcgccc tcttcttcct ggcagaatat gccaacatca tattaataaa cacattgacc
     4741 gccatcttgt ttttcaaccc aagcttccta aaccctcctc aagagttatt ccccgtagta
     4801 ctagccacag aaaccctgct tctatcggca gggttcctat gaatccgcgc ctcctaccct
     4861 cgattccgat acgaccaact aatgcactta ctatgaaaga acttcttacc gctcacacta
     4921 gccctatgtc tatgacacat tagcatgcca atctgctacg caggcctacc accgtaccta
     4981 aga"""

        nd6_pd="""ctaaacagc ccgaatagcc ccccgagata aaccccgcac aagctctaac
     16261 accacaaaca aagtcaacaa caacccccac ccnccaatca acagtaaccc aaccccgtcc
     16321 gaataaagta aggccgcccc gctaaaatcc gaccgaaccg acaacaaacc accattattc
     16381 accgtaccct catccaccaa cagccctacc acaccactca cgacaaaccc cactataaca
     16441 accagaccca taccaaaacc ataaccaaca accccccaac tagcccaagc ctccggataa
     16501 ggatccgctg ctaacgacac cgaataagca aacaccacca acatcccccc cagataaact
     16561 ataacaagca ccaaggacac aaaagaaacc cccaaactaa ccaatcaacc gcaccctgca
     16621 acagcagcca caactaaccc taacacccca taataaggag aagggttaga tgcaactgcc
     16681 aatcccccca aagcgaaaca taaccccaag aacagaacaa attctatcat"""

        nd6_pd = nd6_pd[::-1].replace("a", "T").replace("t", "A")
        nd6_pd = nd6_pd.replace("g", "C").replace("c", "G")

        # Passer Montanus
        cox1_pm="""gtgacat tcatcaaccg atgactattc tcaaccaacc
     5461 acaaagatat cggcaccctg tacctaatct tcggcgcatg agccgggata gtaggtaccg
     5521 ctctaagcct acttatccga gcagaacttg gacaaccagg ggctctccta ggagatgacc
     5581 aagtttacaa cgtagtagtt acagctcacg ctttcgtgat aatcttcttc atagttatgc
     5641 caattatgat cggaggattc ggaaactgac tagtccccct aataattgga gcaccagata
     5701 tagcattccc acgaataaac aacataagct tctgactact gcctccatcc ttcctcctgc
     5761 tactagcatc ctccaccgta gaagcaggag caggcacagg atgaacagta taccccccac
     5821 tggccggcaa cctagcccac gccggagcct cagtagatct agcaatcttc tccctacact
     5881 tagcaggtat ctcatcaatc ttaggggcaa tcaactttat tacaacagca atcaacataa
     5941 aaccccccgc cctatcacaa tatcaaacac ccctatttgt ctgatccgta ctaatcaccg
     6001 cagtactact gctcctgtcg ctacccgtcc tcgctgcagg aatcacaatg ctactcaccg
     6061 accgcaacct caacaccaca ttctttgacc ccgcaggcgg aggagatcca gtcctatacc
     6121 aacatctctt ctggttcttt ggccacccag aagtctacat cctaatcctg cctggattcg
     6181 gaatcatctc ccacgtagta acatactact cagggaaaaa agaaccattc ggctacatgg
     6241 ggatagtgtg agctatgcta tccatcggat tcctgggctt tatcgtctga gcgcatcaca
     6301 tattcacagt aggaatggac gtcgacactc gagcttactt cacatccgcc accataatca
     6361 tcgccatccc aacaggcatc aaagtgttca gctgactagc caccctccac ggaggaacaa
     6421 tcaaatgaga cccaccaata ctgtgagccc taggattcat cttcctattc accattggag
     6481 gactaacagg gattgttcta gcaaactcct cactagacat tgccctacac gacacatact
     6541 acgtggtagc ccacttccac tacgtactat caatgggagc agtattcgca atcctagccg
     6601 gattcaccca ctgattccca ctattcactg gctacactct acactcaaca tgagccaaaa
     6661 cacactttgg ggtaatattc gtgggagtaa acctaacctt cttcccccaa cacttcctag
     6721 gcctagccgg aatgccacga cgatactcag actacccaga cgcctacaca ctatgaaaca
     6781 ctatctcatc agtaggatcg ttaatctctc tgacagccgt aattatgcta gtgttcatca
     6841 tctgagaagc cttcgcatca aaacgcaaag tcctacaacc agaactaaca agcaccaacg
     6901 ttgaatgaat ccacggctgc ccgcccccct tccacacctt cgaagaaccc gcctttgtcc
     6961 aagtacaaga aagg"""

        # Onychostruthus taczanowskii

        nd3_ot = """ata aacataatcc tattcatact aaccctttca
     9601 ttcaccctaa gcatactcct taccgcacta aacttttgac tagcccaaat aaaccccgac
     9661 tcagaaaaac tatccccata cgaatgcgga ttcgaccctc taggatcggc tcgactaccc
     9721 ttctccattc gcttctttct tgtcgccatt ctgttcctcc tattcgacct agaaatcgcc
     9781 ctattactac ccctgccatg agccacccaa ctacaatccc ccatcatcac cctagcctga
     9841 acctccctac tcattcttct tcttacactg ggactaatct atgaatgaat ccaaggagga
     9901 ctagaatgag cagaataa"""

        cox2_ot = """atggccaacc actcacaact taacttccaa
     7141 gacgccgctt cacctattat agaagaacta ataggattcc acgaccacgc tctgatagtt
     7201 gccctagcaa tttgcagcct agtcctttac ctgcttaccc tcatacttac agaaaagcta
     7261 tcatcaagta cagtcaacgc acaagaaatt gagctagttt gaacaatcct accagccata
     7321 gtgctggtca tgctcgccct accctcccta cgaatcctgt acataataga cgaaatcaat
     7381 gaacctgacc taaccctcaa agccatcggt caccaatgat actgatcata cgaatacact
     7441 gacctcaagg acttcacatt tgattcctac ataatcccca caacagacct acccctaggg
     7501 cacttccgcc tactagaagt agaccaccgt gttattgtcc ctataagttc cacaattcga
     7561 gtaatcgtca ctgcagacga cgtacttcac tcatgagccg tccccagcct aggcgtaaaa
     7621 accgacgcaa tcccagggcg cctcaaccaa acctctttcc ttgcctcccg acctggagta
     7681 ttctacggac aatgctcaga aatctgcgga gccaaccaca gcttcatacc aatcgtagta
     7741 gaatctactc ccctcgctaa cttcgaaaac tgatcttctc taatatcatc ctaa"""

        # Prunella strophiata

        atp8_ps = """atgcctcaac taaacccaaa cccttgattt tttatcatgc tcacctcatg actcactttc
     7921 tccctaatta ttcaacccaa actcctaacc ttcgtctcaa tgaatcctcc ctcaaacaag
     7981 atagtaacaa tccctagcac caccccctga acctgaccat gaacctaa"""
    
        genes = [nd1_pd, nd6_pd, cox1_pm, nd3_ot, cox2_ot, atp8_ps]
        genes = [gene.replace(" ", "").replace("\n", "").upper() for gene in genes]
        
        genes_tmp = []

        for gene in genes:
            gene = "".join([nucleotide for nucleotide in gene if not nucleotide.isdigit()])
            genes_tmp.append(gene)
        
        genes = genes_tmp.copy()
        del genes_tmp

        species_path = ["Passer_domesticus/ND1.fasta", "Passer_domesticus/ND6.fasta", "Passer_montanus/COX1.fasta", "Onychostruthus_taczanowskii/ND3.fasta", "Onychostruthus_taczanowskii/COX2.fasta", "Prunella_strophiata/ATP8.fasta"]

        species_path = [path + specie_path for specie_path in species_path]

        # assert some gene seqs
        for gene, path in zip(genes, species_path):
            with open(path, "r") as f:
                gene_file = f.readlines()[1:]
            gene_file = "".join(gene_file).replace("\n", "")
            assert gene == gene_file

        rmtree("Passer_domesticus/")
    


    def test_P_algirus(self):
        original_species = ["Gallotia atlantica", "Psammodromus algirus", "Acanthodactylus aureus", "Acanthodactylus boskianus", "Acanthodactylus erythrurus", "Acanthodactylus guineensis", "Acanthodactylus schmidti", "Algyroides nigropunctatus", "Australolacerta australis", "Darevskia armeniaca", "Darevskia brauneri", "Darevskia caucasica", "Darevskia chlorogaster", "Darevskia clarkorum", "Darevskia daghestanica", "Darevskia dahli", "Darevskia derjugini", "Darevskia mixta", "Darevskia parvula", "Darevskia portschinskii", "Darevskia praticola", "Darevskia raddei", "Darevskia rudis", "Darevskia saxicola", "Darevskia valentini", "Eremias argus", "Eremias brenchleyi", "Eremias dzungarica", "Eremias multiocellata", "Eremias przewalskii", "Eremias stummeri", "Eremias vermiculata", "Lacerta agilis", "Lacerta bilineata", "Lacerta viridis", "Meroles squamulosus", "Mesalina olivieri", "Pedioplanis laticeps", "Phoenicolacerta kulzeri", "Podarcis muralis", "Podarcis siculus", "Takydromus amurensis", "Takydromus sexlineatus", "Takydromus tachydromoides", "Takydromus wolteri", "Zootoca vivipara"]
        original_species.sort()
        original_species = [original_specie.replace(" ", "_") for original_specie in original_species]
        
        for specie in main("Psammodromus algirus", "family", "76e89a45c3498186650efcfc11609d6d9208"):
            pass

        path = "Psammodromus_algirus/genes/"
        species = listdir(path)
        species.sort()

        # delete temp files
        for specie in species:
            temp_file = f"tmp_{specie}"
            if isdir(temp_file):
                rmtree(temp_file)
        
        # assert species names
        for original_specie, specie in zip(original_species, species):
            assert original_specie == specie
        
        # assert num of species
        assert len(original_species) == len(species)

        # assert num of genes for species
        specie_to_rm = "Takydromus_amurensis"
        for specie in [specie for specie in species if not "Takydromus_amurensis"]:
            assert genes_num == len(listdir(path+specie))
        assert 10 == len(listdir(path+specie_to_rm))

        # Psammodromus algirus
        cytb_pa="""a tgacaacaaa cctacgaaaa
    14161 aaccacccaa tcattaaaat cattaacaac tcattcattg acctcccaac ccccccaaac
    14221 atctctgctt gatgaaattt tggatcgctt ctaggattat gcctaattac tcaaattgtt
    14281 acaggactat ttttagcaat acactacaat gcagatatta actccgcatt ctcatcagta
    14341 gcacacattc accgtgatgt ccaacatggc tgacttatcc gaaatattca tgctaacggc
    14401 gcatcactat tctttatttg catttatatg cacatcggac ggggcctata ctatggatcc
    14461 tacttattta ctgaaacatg aaacattgga gtactacttc tcctcttagt aatagccaca
    14521 gccttcatag gttatgtcct cccatgagga caaatatcgt tttgaggggc caccgtcatt
    14581 actaaccttt tatctgccat cccatacatt ggaaacaccc tagttgagtg agtttgaggt
    14641 ggattcgcaa ttgataacgc aacactaact cgattcttca cactacattt cctcctaccc
    14701 tttctgatct taggaacatc actaattcac ctaatatttt tacacgaaac aggatcaaac
    14761 aacccaacag gactaaactc aaactcagat aaaatcccat tccacccata ctactcatat
    14821 aaagacgccc ttggggccct attattcata accctactct tgtacctatc actattctct
    14881 ccattactcc taggagaccc agaaaacttt acaccagcaa acccattagt taccccacca
    14941 cacatcaaac ccgaatgata ctttctattt gcctacgcca ttctccgctc catccccaac
    15001 aaactaggag gtgtactagc ccttctcttc tctatcttaa tcctcctaac aaccccatta
    15061 atacacctgt caaaacaacg atcaaacaca taccgacccc tatcacaagc actcttctga
    15121 ctattaatct cagacatctg cattctaact tgaatcggag gacaacccgt tgaacaccca
    15181 tttatcatta ttggtcaact agcatcttta ctctacttcc tcatcttcct cttacttatg
    15241 cctctaattg ccgtactaga aaataaactc ctcaaatgat aa"""

        nd4_pa="""atgct
    10141 aaaaattatt atccctaccg ccatattagt tccaacagca ttgctattaa acaaagccct
    10201 tttatttcaa actatgactg catactcaat attactagcc ctaacaagcc taacactact
    10261 aaatcaccca ttaaacctaa aaatatacaa actatcaccc tatttatcca ctgacacaac
    10321 ctcatcacca ctaattattc tctcatgctg attactgccg ttaataatac tagccagcca
    10381 aaatcacctt acacatgaac cactaagccg aaagcgcaca ttcttaataa ctgtctcaac
    10441 actacaaaca acattaatca tagcctttgc caccccagaa ctagtaatat tttatattat
    10501 atttgaaaca acattaatcc caaccctgat tataattaca cgttgaggaa atcaaacaga
    10561 acgccttaac gctggcctct acttcttatt ttatacactt gccagctccc tccccctact
    10621 agtcacacta ctttacataa acaataagta tcttaatata tccatagaaa ttatactact
    10681 aaaccaaaca gaacttacaa taaaaacaac acctctacta tggtctgctt gcttactagc
    10741 ctttatagtt aaattacccc tctacggtct ccacctgtga ctcccaaaag cccacgtaga
    10801 agccccaatt gctggatcca tggtactagc cgctatttta ctaaaacttg gtggttacgg
    10861 cctaatccgt gtttcaccaa taatcgcccc taacccacca gcccttatgc ttccaattat
    10921 tatcctagcc ctctgaggca ttatcataac cagctctatt tgcttacgac aaacagacct
    10981 aaaagcacta attgcttatt catctgttag ccacatagga ctagttgcct ctgcaattct
    11041 tatccaaaca ccatgaagct taacaggtgc catagttcta ataattgcac atgggctaac
    11101 ctcatccgcc ctatttaccc tagcaaacac caactatgaa cgaatacaca ctcgaactct
    11161 acttcttata cgaggactac aaattgccct ccccctaata tcaacctgat gactacttac
    11221 aaaccttact aacatagccc tacccccaac aatcaacttg ataggagaat tgcttattat
    11281 ctcagcccta ttcaattgat ccacacccac cattatttta actggaacag gaacccttat
    11341 taccgccaca tactcactat acatattctt aataactcaa cgaggaaccc tcccaacaca
    11401 attacgacaa ctagctccca cacacacccg agaacatctc attataaccc ttcaccttct
    11461 accattgatt cttcttacgc taaagccaag cctaatttcc agctcattca ctt"""

        # Podarcis muralis
        cox1_pm="""gtgtcattag
     5341 ttcgttgact gttttcaacc aatcacaaag acatcggtac cttatatcta ttatttggtg
     5401 cctgagctgg aatagtcggc acagccctta gtcttctaat ccgaacagaa cttagtcaac
     5461 ccggaactct tcttggtgat gatcaggttt ataatgttat tgttacagca catgcttttg
     5521 ttataatttt ctttttagta atacctgtta taattggggg cttcggaaac tgattaatcc
     5581 ccctaataat tggtgcccct gatatggcat ttccacgtat aaataacata agcttctgat
     5641 tattaccacc atctttatta ttacttcttt catcttcagg catcgaagca ggcgctggta
     5701 ctggctgaac tgtctaccca ccattagctg gaaacatagc ccatgcagga gcctcagtag
     5761 atctaacgat cttttcactt catttagccg gagtttcctc tattctaggt gcaattaact
     5821 ttattactac ctgcattaat ataaaacccc ccaacataac gcaatatcaa acgcctctgt
     5881 ttgtatgatc tgttctaatt acagcagtac ttcttcttct ttctcttcct gttttagctg
     5941 ctggcattac tatactgcta acagatcgaa acttaaatac atcctttttt gacccagctg
     6001 gcggagggga tccaattctt taccaacatt tattttgatt ctttggccac ccagaagtgt
     6061 atattcttat tctcccaggt tttggtataa tttctcacat tgtcacatat tatgcaggca
     6121 aaaaagaacc cttcggttat atgggaatag tgtgggctat aatatcaatc ggctttttag
     6181 gctttattgt atgagcccat cacatattta cagtgggaat agatgttgac actcgagctt
     6241 acttcacctc agctacaata attattgcta tccctactgg ggtaaaagtc tttagctgac
     6301 ttgcaacgct ccatggtgga acaattaaat gagacgccgc catactatga gctttaggat
     6361 ttattttttt atttacagta ggaggtctca caggcattat cctagctaac tcctcattag
     6421 atattgtcct tcatgatact tactacgtag ttgctcattt ccactatgtt ctatcgatag
     6481 gcgctgtatt tgcaattata ggcggatttg ttcactgatt tcccctcttt acgggctaca
     6541 ctttacactc ttcatgaaca aaagttcaat ttggcgtaat atttactgga gttaacataa
     6601 cattttttcc acaacatttt cttggattag caggcatgcc tcgacggtac tctgactacc
     6661 cagatgccta cactctttga aacactattt catcaattgg ctccctgatt tccctcacag
     6721 ctgtaattat aataatattt attatttgag aagccctagc agctaaacgt gaagttctta
     6781 ctcttgaatt aataaacaca aacctagagt gacttcacgg ctgtcctcct ccatatcata
     6841 catacgaaga atcaacttat gtacaaacct caagg"""

        # Gallotia atlantica
        atp6_ga = """atggcta taaatctatt tgaccaattt agtatcccct acaccctggg aatcccccta
     7981 attcttatag ccgtactatt cccaccaata attctattct caacaaaccg cttccaccaa
     8041 aaccgaccat catccctgca aacctggtta ttaacaaaca taacaaaaca actcatcctg
     8101 ccaataaacg ccaaaggaca taaatgggcc agcactttca ccgcactatc cacaagcctt
     8161 attctcctta atacattagg acttctacca tatacattca ccccaaccac ccagttgtct
     8221 ataaatatgg ctcttgccat tccctcatga gtaataactg tatttattgg actacgaaac
     8281 caaccaacaa tttccctagg acatctactt ccagaaggca ccccaacccc acttattcca
     8341 atactagtaa taatcgaaac cattagtcta tttattcgcc ccatcgcact aggcgtgcga
     8401 ctcacagcca acctaacagc cggccatctt ttaatacaac taatttcaat agcagtatta
     8461 gccctaataa cgataaccgc cacagctaca atagcaactc tacttacact agtactacta
     8521 tcgtgcctag aaatagcagt tgccctcatt caagcatatg tgtttattct tctattaacc
     8581 ctatatttac aagaaaatat ctaa"""
        
        atp8_ga = """at gccacaactt aacccaaccc catgattctt
     7801 aatcttcgta ctaacctgaa ccacccttct tattattatg ctccctaaaa tcttgaactt
     7861 attcccaacc acacaagcca cctacaatca aaaaaataac ttcaccacaa attgaaattg
     7921 accatggcta taa"""

        # Mesalina olivieri
        nd3_mo = """a tgaacttact
     9421 aattatatta gtaattacca ccctcctatc aactttctta atttttctta gcttttgact
     9481 tccacaaata gtacccgaca cagaaaagct ctccccatat gagtgtggat ttgaccccct
     9541 aggatccgca cgcctcccat tttctctgcg tttttttctg gtcgccattc tttttctact
     9601 ctttgatcta gaaattgccc ttctgctccc aactccttga gcagccaact cctcatgccc
     9661 aacaacaata attttgtgaa caactaccat tattattcta ttgacattag gcctttttta
     9721 tgagtgggtt caagggggcc ttgactgagc cgaata"""
    
        genes = [cytb_pa, nd4_pa, cox1_pm, atp6_ga, atp8_ga, nd3_mo]
        genes = [gene.replace(" ", "").replace("\n", "").upper() for gene in genes]
        
        genes_tmp = []

        for gene in genes:
            gene = "".join([nucleotide for nucleotide in gene if not nucleotide.isdigit()])
            genes_tmp.append(gene)
        
        genes = genes_tmp.copy()
        del genes_tmp

        species_path = ["Psammodromus_algirus/CYTB.fasta", "Psammodromus_algirus/ND4.fasta", "Podarcis_muralis/COX1.fasta", "Gallotia_atlantica/ATP6.fasta", "Gallotia_atlantica/ATP8.fasta", "Mesalina_olivieri/ND3.fasta"]

        species_path = [path + specie_path for specie_path in species_path]

        # assert some gene seqs
        for gene, path in zip(genes, species_path):
            with open(path, "r") as f:
                gene_file = f.readlines()[1:]
            gene_file = "".join(gene_file).replace("\n", "")
            assert gene == gene_file
        
        rmtree("Psammodromus_algirus/")


    def test_B_physalus(self):
        original_species = ["Balaenoptera acutorostrata", "Balaenoptera bonaerensis", "Balaenoptera borealis", "Balaenoptera brydei", "Balaenoptera edeni", "Balaenoptera musculus", "Balaenoptera omurai", "Balaenoptera physalus", "Megaptera novaeangliae"]
        original_species.sort()
        original_species = [original_specie.replace(" ", "_") for original_specie in original_species]
        
        for specie in main("Balaenoptera physalus", "family", api_key):
            pass

        path = "Balaenoptera_physalus/genes/"
        species = listdir(path)
        species.sort()

        # delete temp files
        for specie in species:
            temp_file = f"tmp_{specie}"
            if isdir(temp_file):
                rmtree(temp_file)
        
        # assert species names
        for original_specie, specie in zip(original_species, species):
            assert original_specie == specie
        
        # assert num of species
        assert len(original_species) == len(species)

        # assert num of genes for species
        for specie in species:
            assert genes_num == len(listdir(path+specie))

        # Balaenoptera physalus
        nd2_bp="""ataaac ccattaatcc ttattatcct
     4381 cctgacaacc cttatcctag gtacaataat ggtagtcacc agctctcact gactattagc
     4441 ctgaattggc ttcgaaatga acataatagc cttcatccct atcataataa aaaatcctac
     4501 tccccgggcc acagaagctt ctaccaagta cctcctaaca caagccactg cttccgcact
     4561 cctcataata gcagtcatca ttaacttaat gcactcaggc caatgaacta ttacaaaact
     4621 atttaaccca acagcatcca cactcataac agtagcccta gccatcaaac tgggattagc
     4681 ccccttccac ttttgagttc cagaagtaac acaaggtatc cccctaacca caggcctaat
     4741 cctattaaca tgacaaaaac tagctccctt atcaatccta taccaaattt caccatcaat
     4801 taacctacac ctaatattaa tcatatcctt actttccatc ttaataggag gctgaggtgg
     4861 actaaaccaa acacaacttc gaaaaatcat agcctactca tcaattgccc acataggatg
     4921 aataacggcc attctactat ataatccaac cctaacatta ctaaatctac taatctacat
     4981 cacaataacc ttcaccatat ttatattatt tatccaaaac tcaactacca ctacattgtc
     5041 actgtctcaa acctgaaata aaatacccgt catcacaacc cttaccatac tcactttact
     5101 ctcgatagga ggactcccac cactatcggg gtttataccc aaatgaataa ttattcaaga
     5161 actaacaaaa aatgatatac tcattgtacc aacattcata gccattacag cattactcaa
     5221 cctatacttt tatatacgtc ttacttactc cacagcacta acactatttc cctccacaaa
     5281 taatataaaa ataaaatgac aattcaactc cacaaaacga actcctctcc taccaacagc
     5341 aatcgtaatt tccactatgc tactacccct cacaccaata ctctcaatcc tactatag"""

        nd4l_bp="""at gaccttaatc catataaaca ttctcatagc cttcagcatg
    10381 tcccttatag gcctattaat atatcgatcc cacctaatat ccgcattact ctgcctagaa
    10441 ggtataatac tatcactttt cgtcctagca gcccttacaa tcctaagctc acacttcact
    10501 ttagctaaca tgatgcctat tatcctccta gttttcgcag cttgtgaggc agccatcgga
    10561 ctagccttgc tagttatagt ctctaacaca tatggtaccg attacgtaca aaacctcaac
    10621 cttctccaat gctaa"""

        # Balaenoptera edeni
        cox3_be="""atgaccca ccaaacccac tcataccaca tagtaaatcc cagcccttga
     8701 cctctcactg gagctctatc agcacttctc ataacatcag gcctaattat atgattccat
     8761 ttcaactcaa tagtcctact aactctaggc ttatcaacaa acattctaac aatatatcaa
     8821 tgatgacgag atatcatccg agaaagcacc ttccaaggcc accacacacc aaccgtccaa
     8881 aaaggattac gatacggaat aattctattc attgtctcag aagtcctatt tttcacaggc
     8941 ttcttctgag ccttttacca ttcaagcctt gtccccactc cagaactagg cggatgttga
     9001 ccaccaacag gcatccatcc tttaaatccc ttagaagttc ctctcctcaa tacctccgta
     9061 ctactagcct ctggcgtatc cattacctga gcccaccata gcctaataga aggaaaccgc
     9121 aaacacatac ttcaagccct ctttatcaca attgcactag gcctctactt caccctatta
     9181 caagcatcag aatactacga agcccccttc acaatctcag acggaatcta cggctctacc
     9241 ttctttgtag ccacaggttt tcacggatta catgtaatta ttgggtctac tttccttatc
     9301 gtctgcttcc tacgtcaagt aaaattccac ttcacatcaa gccaccactt cggctttgaa
     9361 gctgccgctt gatactgaca ctttgtagac gtcgtatgat tatttctcta cgtatccatc
     9421 tattgatgag gttcctag"""

        # Megaptera novaeangliae
        atp8_mn = """atgccacaat tagacacatc aacatgactc cttaccatcc
     7861 tatctatact cttaaccctc tttgtactat tccaactaaa aatctcaaag cactcctatt
     7921 cccctagccc caaactaaca cccactaaaa cacaaaaaca acaagctcct tgaaatacca
     7981 catgaacgaa aatttatttg ccccttttat aa"""
        
        nd4l_mn = """atgacctta attcatataa atattctcat
     9961 agccttcagc atatcccttg taggcctatt aatatatcga tcccacctaa tatccgcact
    10021 actctgtcta gaaggtataa tattatcact cttcgtccta gcaaccctca caatcctaag
    10081 ctcacacttc actttagcta acatgatacc tattatcctc ctagtcttcg cagcttgtga
    10141 agcagccatc ggactagcct tgctagtcat agtctccaac acatatggca ctgactacgt
    10201 gcaaaacctc aacctcctcc aatgctaa"""

        # Balaenoptera borealis
        cytb_bb = """at gaccaacatc cgaaaaacac
    14221 acccactaat aaagattgtc aacgatgcat tcgttgatct ccccacccca tcaaatatct
    14281 cctcatgatg aaatttcggc tccctactcg gcctctgctt aattacacaa attctaacag
    14341 gcctattcct agcaatacac tacacaccag acacaacaac cgctttctca tcagttacac
    14401 acatttgccg agacgtaaac tacggctgaa ttatccgata cctacacgca aacggagcct
    14461 ccatattttt catctgcctc tacgcccaca tgggacgagg cctatactac ggctcctacg
    14521 cctttcgaga aacatgaaat attggagtta tcctactatt cacagttata gccaccgcat
    14581 tcgtaggcta cgtcctaccc tgaggacaaa tatcattttg aggcgcaacc gtcatcacca
    14641 acctcttatc agcaatccca tacattggta ctaccctagt cgaatggatc tgaggcggtt
    14701 tctctgtaga taaagcaaca ctaacacgct tttttgcctt ccacttcatt ctccccttca
    14761 ttattctagc actagcaatg gtccacctca ttttcctcca tgaaacagga tccaacaacc
    14821 ccacaggtat tccatccgac atagacaaaa tcccattcca cccttactac acagttaaag
    14881 acattctagg cgccctacta ctaatcctaa ccctactaat actaacccta ttcgcacccg
    14941 acctgcttgg agacccagac aactacaccc cagcaaatcc actcagtacc ccagcacaca
    15001 ttaaaccaga atgatatttc ctatttgcat acgcaatcct acgatcaatc cccaacaaat
    15061 taggcggagt cttagcccta ttactctcaa tcctaatcct agccctaatc ccaatactcc
    15121 acacatctaa acaacgaagc ataatattcc gaccctttag ccaattccta ttttgagtcc
    15181 tagtcgcaga cttactaacc ctgacatgaa tcggtggcca acctgtggaa cacccctatg
    15241 tgatcgtagg ccaatttgca tccatcctct atttcctcct aattctagta ttaataccag
    15301 caactagcct tatcgagaac aaacttataa aatgaaga"""
    
        genes = [nd2_bp, nd4l_bp, cox3_be, atp8_mn, nd4l_mn, cytb_bb]
        genes = [gene.replace(" ", "").replace("\n", "").upper() for gene in genes]
        
        genes_tmp = []

        for gene in genes:
            gene = "".join([nucleotide for nucleotide in gene if not nucleotide.isdigit()])
            genes_tmp.append(gene)
        
        genes = genes_tmp.copy()
        del genes_tmp

        species_path = ["Balaenoptera_physalus/ND2.fasta", "Balaenoptera_physalus/ND4L.fasta", "Balaenoptera_edeni/COX3.fasta", "Megaptera_novaeangliae/ATP8.fasta", "Megaptera_novaeangliae/ND4L.fasta", "Balaenoptera_borealis/CYTB.fasta"]

        species_path = [path + specie_path for specie_path in species_path]

        # assert some gene seqs
        for gene, path in zip(genes, species_path):
            with open(path, "r") as f:
                gene_file = f.readlines()[1:]
            gene_file = "".join(gene_file).replace("\n", "")
            assert gene == gene_file

        rmtree("Balaenoptera_physalus/")

    def test_C_acutus(self):
        original_species = ["Crocodylus acutus", "Crocodylus intermedius", "Crocodylus johnsoni", "Crocodylus mindorensis", "Crocodylus moreletii", "Crocodylus niloticus", "Crocodylus novaeguineae", "Crocodylus palustris", "Crocodylus porosus", "Crocodylus rhombifer", "Crocodylus siamensis", "Mecistops cataphractus", "Osteolaemus tetraspis"]
        original_species.sort()
        original_species = [original_specie.replace(" ", "_") for original_specie in original_species]
        
        for specie in main("Crocodylus acutus", "family", api_key):
            pass
        
        path = "Crocodylus_acutus/genes/"
        species = listdir(path)
        species.sort()

        # delete temp files
        for specie in species:
            temp_file = f"tmp_{specie}"
            if isdir(temp_file):
                rmtree(temp_file)
        
        # assert species names
        for original_specie, specie in zip(original_species, species):
            assert original_specie == specie
        
        # assert num of species
        assert len(original_species) == len(species)

        # assert num of genes for species
        for specie in species:
            assert genes_num == len(listdir(path+specie))

        # Crocodylus acutus
        cox1_ca="""gt gaatattaac cgttgacttt
     5341 tttccactaa ccacaaagat atcggcacct tgtattttat tttcggcgcc tgagccggaa
     5401 tagtaggcac agccataagc ctattaatcc gaacagagct cagccagcca ggtcccttca
     5461 taggagatga ccaaatttac aacgttattg ttacagcaca tgcctttatc ataattttct
     5521 ttatagttat accaattatg atcggaggat ttggaaactg actactccca ttaataatcg
     5581 gggcacccga catagcattc cctcgcataa acaacataag cttctgattg ctgcccccat
     5641 catttaccct acttctcttt tcagccttta ttgaaactgg ggctggcacc ggatgaacag
     5701 tctacccacc actagctgga aacctagccc acgccggacc atcagtagac ctcactatct
     5761 tctcccttca ccttgctgga gtgtcatcca tccttggagc aattaacttt attaccacgg
     5821 ctatcaacat aaaaccccca gcaatgtcac aacaacaaac accccttttt gtatggtctg
     5881 ttctagttac agctgtcctc ctattgctct cactgccagt cctagctgca ggaatcacta
     5941 tactacttac tgaccgaaac ttgaacacca ctttctttga cccagcagga ggaggtgacc
     6001 caatcctata ccaacacctt ttctgatttt tcggccaccc tgaagtatat atccttatcc
     6061 tgccagggtt tggaataatc tcccacgtaa ttacattcta ctcaagcaaa aaagaaccat
     6121 ttggttatat ggggatagtc tgagccataa tgtcaatcgg cttccttgga ttcattgtct
     6181 gagcccacca catgtttaca gtagggatag atgttgatac tcgagcatat ttcacatccg
     6241 ccacaataat tatcgccatc cccactggtg taaaagtatt cagctgatta gccactattt
     6301 acggaggggt agtaaaatga caagccccca tgctctgagc actcggcttc attttcttat
     6361 tcacagttgg aggactaaca ggaattgtac tagctaactc gtcactagac attattctcc
     6421 acgataccta ctacgtagta gcccacttcc actatgtatt atctatggga gcagtgttcg
     6481 ccatcataag cgggttcacc cactgattcc cgctattcac aggattcacc ctacacagca
     6541 catgaacaaa aattcaattc ataatcatat ttacaggtgt aaatctaacc ttcttcccac
     6601 aacacttcct aggcctatca gggataccac gacgatattc agactaccca gatgcatacg
     6661 ccttctgaaa tataatttcc tccattgggt cattaatttc catagtatca gttatcctac
     6721 tcacatttat tgtatgagaa gcattttcgt caaaacgaaa agtccaagta cctgaaatag
     6781 caagcacaaa tgtagaatga ctaaacaatt gtccaccatc atatcacacc tacgaagaac
     6841 cagtctttgt tcaagtacaa ccaaaattaa cataa"""

        cox2_ca="""atggcaaacc caatacacct aggactccaa gatgcaatat
     7081 ccccactaat agaagaactc ctctactttc atgaccatac actaataatt attttcctaa
     7141 tcagcatgtt tgtactttat acaatctcag ttttattact aacaaaccta taccacacaa
     7201 atgcaacaga tgtacaagaa atagaaataa tctgaaccat tctaccagcc ctaatcctga
     7261 ttaccatcgc ccttccatcc ctacgcacgc tatacctcat agacgaaacc accaacccct
     7321 gcctaaccat taaagtcatc ggacatcagt ggtattgaac atatgaatat acagactttt
     7381 cacagctgga atttgactcc tatatactac caacacaaga cctacctcaa ggtcatttcc
     7441 gcctcttaga agtagaccac cgcatggttg tccccacaaa ctcaagcact cgaacactaa
     7501 tcacagctga agacgtccta cactcatgag cagtaccatc cttaggaatc aaaatagacg
     7561 cggtgccggg gcgactaaat caaacctcac taacatctcc caaccctggg gttttctatg
     7621 gccaatgctc tgaaatctgt ggggcaaacc atagttttat gcctattgtc gtagaagctg
     7681 taccaataca gcacttccaa agctgattaa aaacaaactc ataa"""

        # Crocodylus johnsoni
        cox3_cj="""at
     8641 gacccaccaa acacacctgt ttcacatagt caacccaagc ccatggccaa ttacaggagc
     8701 catagccgcc ataatattaa caactggatt aatcctatga ttccactata attcaagcct
     8761 aattctaatt ctaggactaa tctccacctt actgactata tttcaatgat gacgagacgt
     8821 tgtccgagaa agcacctatc taggccacca taccccacca gtccaaaaag gactacgata
     8881 tggcataatc ctctttatca tctcagaggt tttctttttc ctcggattct tctgagcgtt
     8941 ctatcactca agcttggccc caaccccaga actaggagga cagtgaccac caactggaat
     9001 ttccacacta gacccattcg aagtcccgct cctcaacaca gcagtactat tagcctcagg
     9061 agttacagta acatgagccc accacagcct aatagaagcc aaccgagcac ccgccatcca
     9121 cgccttaatt cttacaattg ccctaggact atacttcact gctcttcaag caatagaata
     9181 ctacgaagcc ccattcacca tcgcagacag cagctatggg tcaaccttct ttgttgccac
     9241 aggctttcac ggcctacacg ttattatcgg atcaacattc ctaataacct gcctctatcg
     9301 acaaatcatg caccacttca cctcaaatca ccatttcggt ttcgaagctg ccgcttgata
     9361 ctgacacttc gtagatgtag tctgactgtt cctgtacatc tcaatctatt gatgaggctc
     9421 ct"""

        # Mecistops cataphractus
        nd1_mc = """attagtttta taacggctgc cccaatcata ctttatatca
     2761 ttttaatcct agttgcagtt gcattcctta ctggcctaga acgaaaaatc attggttaca
     2821 tacaactacg aaaggggcct aatattgtag ggccactggg cctactacaa ccatttgctg
     2881 atggcctcaa acttattatc aaagaactaa cactccccct actcgccacc ccaaccctat
     2941 ttgttttgtc ccctgcagtg gccctcatcc tatccttaat tatgtgagca cccttacccg
     3001 taccattctc catcgccaac ctaaaccttg gcatactgtt cttactagcc atatctagct
     3061 tagcagtcta ttccttatta tgatccggct gagcatcaaa ctctaaatac gcactaatag
     3121 gggcattacg ggcagtagcc caaactatct catatgaagt tacactagcc atcattgtcc
     3181 tatccattgt gctgctcagt ggcggatttt cactacacgc actcaccatt acccaagaac
     3241 cgctcttcct agcattaacc acatgacctt tactaataat atggtatacc tcaacactag
     3301 cagaaactaa ccgcgcccca tttgacttaa cagaaggtga gtcagaacta gtatccggat
     3361 tcaacgtaga gtacagcgca ggattattca cactgttctt tttggccgaa tacgccaaca
     3421 tcttattgat aaatgtttta accaccatcc tgttcctaaa catacctata aatctaccaa
     3481 cacaaacact attcaccgca gctttaatag gcaaagctat cacgctaacc gtgggttttc
     3541 tctgggtacg agcatcatac ccacgattcc gctatgacca gctaatacac ctcctatgaa
     3601 aaagcttctt acccgtcaca ttagcaatat gcctatgaca ctcaacacta ccaatatcaa
     3661 tatttggtct accggtaaca agg"""
        
        nd2_mc = """atg
     3901 cccatcttcc aacccattat tttaaccaca ctaaccatca ccacattcat ttttctatcc
     3961 tccacccacc tagtacttat gtgagtatca ctggaactta gcacgctagt agtcttacca
     4021 ctaattgcca ataaatcaca cccccgagca attgaagcct ccacaaaata cttcctgaca
     4081 caagccaccg cttctgcact aataatcttc tcagggacat taaactatat taccaccgga
     4141 aactgtcaaa ttatagaaat aacaaaccaa accctaataa tcattataat cctagccatg
     4201 tttattaaag tcggattagt gcctttccac ttctgggtgc cagaaactat tcaaggaata
     4261 tccccaaccg cctcaatctt cctgctaacc tggcaaaagc tgggcccact aatcttatta
     4321 ttcctaataa gcccattagt taattttgaa gtaatctctg tggtatctat cctatctgtt
     4381 acagttgctg ggtgactcgg actcaaccaa acccagatcc gtaaactggt agcactctcc
     4441 tcaatcgctc agatagcttg aaccgttatt atcatcaaat atgcaccatc acttacaatt
     4501 ctagctttct atgtatactc aattaccatt tccacaaccc tactcacgct agaaaagcta
     4561 tcaacaacct ctgttagcag cctgctactt tcattctcaa aagccccaat tacctcatca
     4621 ttactgacaa tctccctact atccctgtca ggacttccac cactagccgg attcctccca
     4681 aaatgattaa ccattgacca actagtggca gaaggggcaa tttgaattac attcgtaatg
     4741 ctcatggctt cccttctaag cctattcttc tacctacgtt tgtgatacaa ctccgcatcc
     4801 accctgccac caaacgctac caacaccaaa cgactatgac gtaaatcaac ccaacaaacc
     4861 aaccttacaa tcaactcaat agccacagcc gctataaccc taatcctagc agccacctta
     4921 ataaaagcca ttacaaaaca agacattaat taa"""

        # Osteolaemus tetraspis
        nd3_ot = """ata aacttactca ccctgtttat
     9541 attagccatg gctactgcaa cagctgtaat tgccctaaac ctattaatat cagaacccac
     9601 cccagattca gaaaaactct caccgtacga gtgtgggttt gacccattag ggtcagctcg
     9661 ccttccacta tctatccgct tttttataat tgccattcta ttcttattgt tcgacctaga
     9721 aatcgccatc ctactgccac tagcatgggc actccaacta tcaaacctga ttaaaactac
     9781 tgtattaacc atcactatct tctcactcat atttacgggt ctagcatatg agtgactaca
     9841 aggaggttta gaatgagcag aataa"""
    
        genes = [cox1_ca, cox2_ca, cox3_cj, nd1_mc, nd2_mc, nd3_ot]
        genes = [gene.replace(" ", "").replace("\n", "").upper() for gene in genes]
        
        genes_tmp = []

        for gene in genes:
            gene = "".join([nucleotide for nucleotide in gene if not nucleotide.isdigit()])
            genes_tmp.append(gene)
        
        genes = genes_tmp.copy()
        del genes_tmp

        species_path = ["Crocodylus_acutus/COX1.fasta", "Crocodylus_acutus/COX2.fasta", "Crocodylus_johnsoni/COX3.fasta", "Mecistops_cataphractus/ND1.fasta", "Mecistops_cataphractus/ND2.fasta", "Osteolaemus_tetraspis/ND3.fasta"]

        species_path = [path + specie_path for specie_path in species_path]

        # assert some gene seqs
        for gene, path in zip(genes, species_path):
            with open(path, "r") as f:
                gene_file = f.readlines()[1:]
            gene_file = "".join(gene_file).replace("\n", "")
            assert gene == gene_file

        rmtree("Crocodylus_acutus/")

    
    def test_G_camelopardalis(self):
        original_species = ["Giraffa camelopardalis", "Giraffa giraffa", "Okapia johnstoni"]
        original_species.sort()
        original_species = [original_specie.replace(" ", "_") for original_specie in original_species]
        
        for specie in main("Giraffa camelopardalis", "family", api_key):
            pass

        path = "Giraffa_camelopardalis/genes/"
        species = listdir(path)
        species.sort()

        # delete temp files
        for specie in species:
            temp_file = f"tmp_{specie}"
            if isdir(temp_file):
                rmtree(temp_file)
        
        # assert species names
        for original_specie, specie in zip(original_species, species):
            assert original_specie == specie
        
        # assert num of species
        assert len(original_species) == len(species)

        # assert num of genes for species
        for specie in species:
            assert genes_num == len(listdir(path+specie))

        # Giraffa camelopardalis
        nd4_gc="""atgctaaaat atattatccc tacaataata
    10201 cttatacccc tgacttgatt atcaaaaaat aacataatct gagtcaactc cacagctcac
    10261 agcctactaa ttagcctcac tagtttactc cttactaatc aattcagtga taacagcctt
    10321 aacttctcac tagtcttctt ctccgactct ctatccacac cactactgat cctgaccata
    10381 tgacttctcc ctttaatact aatagccagc caacatcatc tatcaaaaga aaatctgacc
    10441 cgaaaaaaac tatatattac tatactaatt ctactacaac tattcctaat tatgaccttt
    10501 gccgccatag agctaatcct cttttatatt ctatttgaag ccacactaat cccaacactt
    10561 atcattatta cccgatgagg taatcaaaca gaacgcctaa atgccggtct ctacttttta
    10621 ttttacacac tagcaggctc cctcccacta ctagtcgcac taacctatat ccaaaacata
    10681 gtaggatctc taaactttct agtccttcaa tactgagcgc aaccagtatc caattcctga
    10741 tcaaatatct tcatatgact agcatgtata atagccttta tagtaaaaat accactatac
    10801 ggcctccacc tctgactacc caaagcccat gtagaagccc ccattgcagg ctccatagtc
    10861 cttgcagcag tcctactaaa actaggagga tatggcatac tacgaatcac aacattacta
    10921 aacccattaa ccgacttaat agcatatcca tttatcatat tatccttatg gggtataatt
    10981 ataaccagtt caatttgcct ccgccaaaca gacctaaaat cactaattgc atactcctct
    11041 gtcagccata tagcactagt tatcgtagcg atccttattc aaacaccctg aagctacata
    11101 ggagccatgg ccttaataat cgcccatggt ctcacatcct ctatactctt ctgcctagca
    11161 aactctaact acgaacgaat tcatagccga acaataatcc tcgcccgagg cctgcaaaca
    11221 ctcctcccac taatagccac atgatggctt ctagcaagcc taaccaactt ggctctaccc
    11281 ccaacaatta acctaattgg agaactactt gtaataatat cagctttctc atgatccaac
    11341 gccacaatta tcctaatagg ggtaaacata gtgattactg ccctgtactc cctatatatg
    11401 ctaatcacca cacaacgagg taaacatacc caccacatca ataatattac gccctctttc
    11461 acacgagaaa atgctcttat atcattacat atcttacccc tactacttct atccttaaac
    11521 cccaaaatca tcctaggccc cctgtact"""

        nd5_gc="""at aaacctattt
    11761 tcctctctct cactaaccac actattctta ttaactatcc ccatcataat aacaggctcc
    11821 agtacttata aaacctcaaa ctacccactc tatgtaaaaa cagccatctc atgtgctttc
    11881 gctaccagca tagtccccac aataatattc attcatacag gacaagaaat agtaatttca
    11941 aactgacact gacttacaat ccaaaccatc aaactatcac tcagctttaa aatagactac
    12001 ttctctataa tattcatccc agtggcactg ttcgttacat ggtccatcat agaattctca
    12061 atctgatata tacactcaga cccaaacatt aaccaatttt tcaagtacct tctcctattt
    12121 ctcattacta tactcatcct cgttaccgca aacaacctgt tccaactatt cattggctga
    12181 gaaggagttg gaattatatc gttcctactc atcggatgat ggcacggacg ggcagatgca
    12241 aacacagcag ctctacaggc aatcctatat aatcgcatcg gcgatatcgg atttattctg
    12301 gcaatagcat gatttcttgc taacctcaac gcctgggact ttcaacaaat ctttatacta
    12361 agcccaaaca actccaacct acccctaata ggccttgtac tagccgcagc cggaaaatcc
    12421 gctcaatttg gtctacaccc ttgactgccc tctgcaatag aaggccctac tcctgtctca
    12481 gcactacttc actcaagcac aatagtagta gcgggcgtct tcctactaat tcgcttccac
    12541 ccactaatag aaaacaacaa actcattcag tctattacac tatgcctagg agctattacc
    12601 acattattta cagcaatatg tgcccttacc caaaatgaca tcaaaaaaat cattgccttc
    12661 tctacatcaa gccaactagg tctcataata gtaacaatcg gtattaacca gccctaccta
    12721 gcatttcttc atatttgtac ccacgccttc tttaaggcca tactattcat atgctccggc
    12781 tccattatcc acaacctaaa caatgaacaa gatatccgaa aaataggagg cctattcaaa
    12841 gccataccat ttaccacaac agccctcatt attggcagcc ttgcattaac aggaatacct
    12901 ttccttaccg gattctactc taaagactta attatcgaaa ccgccaatac gtcaaatacc
    12961 aacgcctggg ccctcttaat aacactaatc gccacctcct ttacagcaat ctacagtact
    13021 cgcactattt tcttcgcact cctaggacaa ccccgatttc caaccctaat tactatcaac
    13081 gaaaataacc ctactcttac taaccccatt aaacgcctac taatcggaag cctttttgca
    13141 ggatttatca tctccaatag cattccccca acaacaatcc cacaaataac catgccccaa
    13201 tacctaaaaa taacagccct agcagtaaca atcctaggtt ttattctagc actagaaatc
    13261 agcaacataa cccaaaatct aaaattcagc cacccatcaa actcctttaa attctccaac
    13321 cagctagggt attttcctac aatcatacac cgcctaattc cccacgcaag cctaacaata
    13381 agccaaaaat cagcatcctc cctgctagac ttaatctgac tagaaaacat cttaccaaaa
    13441 accacctcgc taatccaaat aaaaatatcc atcatcatta caaaccaaaa aggcctaatc
    13501 aaatcatact tctcttcctt cctaattaca atcctcatta gcataatcct atttaatttc
    13561 cacgagtaa"""

        # Giraffa giraffa
        nd6_gg="""ttaatttc
    13561 cacgagtaat ttctataata accacaacac caacaaacaa cgaccaccca gtcacaataa
    13621 ccaaccaagt tccataacta tacaaagctg caatccccat agcctcctca ctaaaaaatc
    13681 cagaatcccc cgtatcatag ataacccaat ctcccacacc attaaactta aaaacaatct
    13741 ccacttcctc atctttcaac atataataaa ccatcaaaaa ctctattaac agaccgacaa
    13801 taaatgcccc taaaacagtt ttattanaag cccaaacctc aggatactgc tcaatagcta
    13861 tagccgttgt atagccaaaa actaccatta taccccccag ataaattaaa aaaaccatca
    13921 aacctaaaaa agacccacca aaatttaaca caatcccaca accaacccca ccactcacaa
    13981 tcaaccccaa ccccccataa ataggcgaag gttttgaaga aaaacccaca aaaccaatca
    14041 caaaaataac actcaaaata aacacaatgt acactatcat"""

        nd6_gg = nd6_gg[::-1].replace("a", "T").replace("t", "A")
        nd6_gg = nd6_gg.replace("g", "C").replace("c", "G")

        nd4l_gg = """a tgtctctagt ccatataaat
     9901 atcataacag catttgcggt atctcttaca ggattattaa tataccgatc ccacctaata
     9961 tcatcccttt tatgtctaga aggaatggta ttatccctat tcgttatagc caccctaatt
    10021 atcctaaact cgcacttcac cctagctagc ataataccta tcatcctgct agtattcgca
    10081 gcctgtgaag cagccctagg attatcccta ctagttatag tatcaaacac atatggcacc
    10141 gactacgtac aaaatcttaa cctgctccaa tgctaa"""

        # Okapia johnstoni
        atp6_oj = """atgaacga aaatctattt gcctctttta ttaccccaac attcctaggc
     7981 ctccctcttg tcaccctcat catcatattc cctagcctat tattcccaac atcaagccga
     8041 ctagtaagca atcgtttgat ttccctccaa caatgagcac tccaactcat atcaaaacaa
     8101 ataataggca ttcacaacac caaagggcaa acatggaccc taatattaat atccctaatt
     8161 ctatttatcg gcacaacaaa tctactaggc ctgctacccc actcatttac accaaccaca
     8221 caattgtcaa taaatctagg catagccatc cccctatgag caggagctgt agccacagga
     8281 tttcgcaata aaaccaaagc atcactcgcc cacctcctac cacaaggaac accaacccct
     8341 ttaatcccaa tactagtaat cattgagacc atcagtctct ttattcaacc agtggcctta
     8401 gccgttcggc taacagctaa tattaccgcc ggacatttgc taattcacct gattggagga
     8461 gccacactcg cactaataaa cattagtgcc ataacagccc tcatcacatt tattgtttta
     8521 gtcctactaa caattctcga attcgcagta gccataattc aggcctacgt gttcaccctc
     8581 ctggttagct tatacttgca cgacaacaca taa"""

        atp8_oj = """atgccacag ctagacacat ccacatgatt
     7801 tacaattatc ctatctatat ttatagccct cttcattata tttcaactaa agatctcaaa
     7861 acataatttt tattccgacc cagaactgat cttaaccaaa acacacaaac aaaacacccc
     7921 ttgagaaaca aaatgaacga aaatctattt gcctctttta ttaccccaac attcctag"""
    
        genes = [nd4_gc, nd5_gc, nd6_gg, nd4l_gg, atp6_oj, atp8_oj]
        genes = [gene.replace(" ", "").replace("\n", "").upper() for gene in genes]
        
        genes_tmp = []

        for gene in genes:
            gene = "".join([nucleotide for nucleotide in gene if not nucleotide.isdigit()])
            genes_tmp.append(gene)
        
        genes = genes_tmp.copy()
        del genes_tmp

        species_path = ["Giraffa_camelopardalis/ND4.fasta", "Giraffa_camelopardalis/ND5.fasta", "Giraffa_giraffa/ND6.fasta", "Giraffa_giraffa/ND4L.fasta", "Okapia_johnstoni/ATP6.fasta", "Okapia_johnstoni/ATP8.fasta"]

        species_path = [path + specie_path for specie_path in species_path]

        # assert some gene seqs
        for gene, path in zip(genes, species_path):
            with open(path, "r") as f:
                gene_file = f.readlines()[1:]
            gene_file = "".join(gene_file).replace("\n", "")
            assert gene == gene_file

        rmtree("Giraffa_camelopardalis")
