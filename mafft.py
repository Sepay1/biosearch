#!/usr/bin/env python3

from os import system, listdir, mkdir
from os.path import isdir
from sys import argv


def create_path(path):
    if isdir(path):
        print(f"{path} already exists.")
        return 1
    else:
        mkdir(path)
     

def append_same_genes(species, list_species, genes_files):
    path = f"{species}/disordered_genes/"
    error = create_path(path)
    if error:
        return
    for genes_file in genes_files:
        for one_species in list_species:
            try:
                with open(f"{species}/genes/{one_species}/{genes_file}") as gene:
                    fasta = gene.read()
            except FileNotFoundError:
                print(f"{one_species} has not {genes_file} file.")
                continue
            with open(f"{path}{genes_file}", "a") as gene:
                gene.write(fasta + "\n")


def mafft(species, genes_files):
    # system(f"mafft --auto {species}")
    disordered_path = f"{species}/disordered_genes/"
    ordered_path = f"{species}/ordered_genes/"
    error = create_path(ordered_path)
    if error:
        return
    
    for genes_file in genes_files:
        system(f"mafft --thread -1 --auto {disordered_path}{genes_file} > {ordered_path}/{genes_file}")


def main(species):
    species = "_".join(species.split())
    genes_files = listdir(f"{species}/genes/{species}/")
    # genes = [gene.split(".")[0] for gene in genes]
    all_species = listdir(f"{species}/genes/")
    append_same_genes(species, all_species, genes_files)
    mafft(species, genes_files)


if __name__ == "__main__":
    main(argv[1])
    
