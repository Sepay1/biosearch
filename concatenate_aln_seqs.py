#!/usr/bin/env python3


from sys import argv
from os import listdir, mkdir
from os.path import isfile
# from ncbi_downloader import write_to_a_file


def get_num_genes_in_files(path, genes_files):
    num_genes_in_files = []
    for file in genes_files:
        with open(path+file, "r") as f:
            genes = f.readlines()
        num_genes = len([gene for gene in genes if gene.startswith(">")])
        num_genes_in_files.append(num_genes)
    return num_genes_in_files


def get_name_species(path, genes_files, num_genes_files):
    max_genes = max(num_genes_files)
    
    species = []

    for file, num_genes in zip(genes_files, num_genes_files):
        if num_genes == max_genes:
            with open(path + file, "r") as f:
                species = [" ".join(line.split()[1:]) for line in f if line.startswith(">")]
                species = [specie.split(",")[0] for specie in species]
            return species


def filter_data(path, genes_files, num_genes_files):
    max_genes = max(num_genes_files)

    species = get_name_species(path, genes_files, num_genes_files)

    filtered_files = []
    species_to_remove = []
    
    for file, num_genes in zip(genes_files, num_genes_files):
        if num_genes < max_genes//2:
            continue
        
        elif num_genes < max_genes:
            with open(path + file, "r") as f:
                species_tmp = [" ".join(line.split()[1:]) for line in f if line.startswith(">")]
                species_tmp = [specie.split(",")[0] for specie in species_tmp]
                
            for specie in species:
                if specie not in species_tmp:
                    species_to_remove.append(specie)
                
        filtered_files.append(file)
    
    species_to_remove = list(set(species_to_remove))
    return filtered_files, species_to_remove
    

def get_list_genes(path, genes_file, species_to_remove):
    with open(path + genes_file, "r") as f:
        genes_file = f.read()
    
    genes_file = genes_file.split(">")
    genes_file = genes_file[1:]
    
    for specie_to_remove in species_to_remove:
        gene_to_delete = [gene for gene in genes_file if specie_to_remove in gene]
        try:
            genes_file.remove("".join(gene_to_delete))
        except ValueError:
            pass

    genes_file = [">" + gene for gene in genes_file]
    # genes_file = "".join(genes_file).split("\n")
    return genes_file


def write(data, path, filename):
    try:
        with open(path + filename, "a") as f:
            f.write(data)
    except FileNotFoundError:
        mkdir(path)
        write(data, path, filename)


def main(species):
    path = f"{species}/ordered_genes/"

    aln_genes_files = listdir(path)
    aln_genes_files = [aln_genes_file for aln_genes_file in aln_genes_files if aln_genes_file.endswith(".fasta")]
    
    num_genes_in_files = get_num_genes_in_files(path, aln_genes_files)
    aln_genes_files, species_to_remove = filter_data(path, aln_genes_files, num_genes_in_files)
    all_species = get_name_species(path, aln_genes_files, num_genes_in_files)
    
    for specie_to_remove in species_to_remove:
        all_species.remove(specie_to_remove)

    super_path = f"{species}/super_aln_genes/"

    for file in aln_genes_files:
        gene_file = get_list_genes(path, file, species_to_remove)

        for index, specie in enumerate(all_species):
            specie = "_".join(specie.split())
            gene = gene_file[index].split("\n")[1:]

            if not isfile(super_path + specie + ".fasta"):
                gene = ">" + specie + "\n" + "\n".join(gene)
            else:
                gene = "\n".join(gene)

            write(gene, super_path, specie + ".fasta")
    
    aln_super_files = listdir(super_path)
    
    for super_file in aln_super_files:
        with open(super_path + super_file, "r") as f:
            super_sequences = f.read()
        write(super_sequences, super_path, "all_super_species.fasta")


if __name__ == "__main__":
    main(argv[1])
