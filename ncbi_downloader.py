#!/usr/bin/env python3

from simplejson.errors import JSONDecodeError
import requests as r
from sys import argv, stderr
from os import makedirs, listdir
from os.path import isfile, isdir
from shutil import rmtree
from bs4 import BeautifulSoup
import pickle
from time import time
import traceback
from multiprocessing import Pool
from functools import partial


def get_ncbi_data(api, parameters, wtaf=False, output_file="file.xml"):
    """A function to get data from ncbi

    Args:
        api (string): Type of API from NCBI ("esearch.fcgi" or "efetch.fcgi" p.e.)
        parameters (dict): Parameters of NCBI search in format param: param_value
        wtaf (bool, optional): If True writes to output_file. Defaults to False
        output_file (str, optional): Name of the file if wtaf. Defaults to "file.xml"

    Returns:
        str: XML file in a str format
    """
    while True:
        response = r.get(f"https://eutils.ncbi.nlm.nih.gov/entrez/eutils/{api}", params=parameters)
        try:
            error = response.json()
            if error["error"] == "API rate limit exceeded":
                print("API rate limit exceeded, trying again.")
                continue
            else:
                print("Another error that not API error ocurred.")
        except (KeyError, JSONDecodeError):
            break
    if wtaf:
        write_to_a_file(response.text, output_file)
    return response.text


def write_to_a_file(input_data, output_file):
    """Function to write data to a file

    Args:
        input_data (str): Data to write to a file if
        output_file (str): Name of the file
    """
    output_file = "_".join(output_file.split(" "))
    counter=1
    while True:
        if isfile(output_file):
            output_file = output_file.split(".")
            name_file = output_file[0]
            extension = output_file[1]
            if counter > 1:
                name_file = name_file[:-len(str(counter))-1]
            counter += 1
            output_file = name_file + "_" + str(counter) + "." + extension
        else:
            break
    try:
        with open(f"./{output_file}", "w") as file:
            file.writelines(input_data)
    except FileNotFoundError:
        output_dir = "/".join(output_file.split("/")[:-1])
        makedirs(output_dir)
        with open(f"./{output_file}", "w") as file:
            file.writelines(input_data)


def write_pickle(input_data, output_file):
    """Similar to write_to_a_file but works with pickle

    Args:
        input_data (dict): A Python dictionary and in this case in format genes_info[gene_name] = (accn, (gene_start, gene_end), complementar)
        output_file (str): The name of the file
    """
    output_file = "_".join(output_file.split(" "))
    counter=1
    while True:    
        if isfile(output_file):
            output_file = output_file.split(".")
            name_file = output_file[0]
            extension = output_file[1]
            if counter > 1:
                name_file = name_file[:-len(str(counter))-1]
            counter += 1
            output_file = name_file + "_" + str(counter) + "." + extension
        else:
            break
    try:
        with open(f"./{output_file}", "wb") as file:
            pickle.dump(input_data, file)
    except FileNotFoundError:
        output_dir = "/".join(output_file.split("/")[:-1])
        makedirs(output_dir)
        with open(f"./{output_file}", "wb") as file:
            pickle.dump(input_data, file)


def parse_one_xml(soup="", xml_data="", path=""):
    """Function that gets a soup of BeautifulSoup or a string with XML data, and returns the content of soup (or xml_data) with the path equals to path (first occurrence)

    Args:
        soup (str, optional): A BeautifulSoup soup. Defaults to "".
        xml_data (str, optional): XML data. Defaults to "".
        path (str, optional): Path of the tags of BeautifulSoup data (or XML data). Defaults to "".

    Returns:
        bs object: The first value of xml_data with path equals to path
    """
    if not soup:
        soup = BeautifulSoup(xml_data, "html.parser")
    return soup.select_one(path) if path else soup

def get_one_text(soup="", xml_data="", path=""):
    """Similar to parse_one_xml but returns in str instead of bs object

    Args:
        soup (str, optional): A BeautifulSoup soup. Defaults to "".
        xml_data (str, optional): XML data. Defaults to "".
        path (str, optional): Path of the tags of BeautifulSoup data (or XML data). Defaults to "".

    Returns:
        str: The first value of xml_data with path equals to path
    """
    try:
        return parse_one_xml(soup, xml_data, path).get_text()
    except AttributeError:
        pass

def parse_all_xml(xml_data, path):
    """Gets xml_data and returns all the occurrences of xml_data with path equals to path

    Args:
        xml_data (str): XML data
        path (str): path of xml_data delimited by a with space

    Returns:
        bs object: All the values of xml_data with path equals to path
    """
    soup = BeautifulSoup(xml_data, "html.parser")
    return soup.select(path)


def get_querykey_webenv(api="", parameters="", xml_data="", wtaf=False, output_file="file.xml"):
    """Function that returns the querykey and the webenv values of a search in NCBI.

    Args:
        api (str, optional): API of NCBI, in this case eseacrh.fcgi. Defaults to "".
        parameters (dict, optional): Parameters of the search. Defaults to "".
        xml_data (str, optional): If the search was been done before put the XML data here, if not use api and parameters instead. Defaults to "".
        wtaf (bool, optional): If wants to write to a file or not. Defaults to False.
        output_file (str, optional): If wtaf is True, it's the name of the file. Defaults to "file.xml".

    Returns:
        tuple: Tuple with two values the value of querykey and of webenv
    """
    if not xml_data:
        xml_data = get_ncbi_data(api, parameters, wtaf, output_file)
    soup = BeautifulSoup(xml_data, "html.parser")
    return get_one_text(soup=soup, path="QueryKey"), get_one_text(soup=soup, path="WebEnv")


def get_gene_from_genome(genome, start, end):
    """Gets the genome, the start and end coordenates, counting from 0

    Args:
        genome (str): Whole genome with the genes
        start (int): coords where gene starts
        end (int): coords where gene ends

    Returns:
        str: Gene
    """
    gene_seq = genome[start:end]
    return gene_seq
    
    
def format_gene(gene_seq):
    """Formats the gene sequence to fasta format

    Args:
        gene_seq (str): Raw gene

    Returns:
        str: Formated gene
    """
    len_line = 70
    sequence_len = len(gene_seq)
    gene_seq = [gene_seq[part*len_line:part*len_line+len_line] + "\n" for part in range(sequence_len//len_line + 1)]
    gene_seq = "".join(gene_seq)
    return gene_seq


def get_complement_gene_sequence(gene_seq):
    """Gets the gene sequence

    Args:
        gene_seq (str): Gene sequence

    Returns:
        str: Complement gene sequence
    """
    complementary_nucleotides = { # COMPLETE THIS DICT
        "A": "T",
        "T": "A",
        "C": "G",
        "G": "C",
        "N": "N"
    }
    gene_seq = gene_seq.strip()[::-1]
    gene_seq = [complementary_nucleotides[nucleotide] for nucleotide in gene_seq]
    gene_seq = "".join(gene_seq)
    return gene_seq


def get_genes_info(esearch_api, efetch_api, esearch_parameters, efetch_parameters, specie, study_specie, genes_list=""):
    """Function that writes to tmp file with pickle the genes in the

    Args:
        esearch_api (str): esearch_api, in this case "esearch.fcgi"
        efetch_api (str): efetch api, in this case "efetch.fcgi"
        esearch_parameters (dict): esearch parameters
        efetch_parameters (dict): efetch parameters
        specie (str): Species to get the data
        study_specie (str): species dir of the specie in study
        genes_list (list of str, optional): list with the name of the genes to search. Defaults to "".

    Raises:
        ValueError: Raises an error mensage if not encounters genes when searching for the species

    Returns:
        None: None
    """
    esearch_parameters["db"] = "gene"
    efetch_parameters["db"] = "gene"
    print(specie)
    if genes_list:
        esearch_parameters["term"] = specie + "[organism] AND (" + " OR ".join(genes_list) + ")"
    else:
        esearch_parameters["term"] = specie + "[organism]"
    efetch_parameters["retmode"] = "xml"
    if  "rettype" in efetch_parameters:
        del efetch_parameters["rettype"]
    esearch = get_ncbi_data(esearch_api, esearch_parameters)
    efetch_parameters["query_key"], efetch_parameters["WebEnv"] = \
    get_querykey_webenv(esearch_api, esearch_parameters, esearch)
    count = int(get_one_text(xml_data=esearch, path="Count"))
    retstart = 0
    retmax = 800
    efetch_parameters["retmax"] = str(retmax)
    cont = 1
    while True:
        genes_info = {}
        efetch_parameters["retstart"] = str(retstart)
        efetch_gene_names = get_ncbi_data(efetch_api, efetch_parameters, wtaf=True, output_file=f"{study_specie}/xml/{specie}/{specie}_genes.xml")
        
        entrezgenes = parse_all_xml(efetch_gene_names, "Entrezgene")
        entrezgenes = [entrezgene for entrezgene in entrezgenes if entrezgene.entrezgene_type["value"] == "protein-coding"]
        # if we want RNA in the future delete the line above
        try:
            error_msg = get_one_text(xml_data=efetch_gene_names, path="ERROR")
            if error_msg:
                raise ValueError
        except ValueError:
            print(f"The {specie} species has no genes.")
            specie = "_".join(specie.split(" "))
            rmtree(f"{study_specie}/xml/{specie}")
            return 1

        for entrezgene in entrezgenes:
            gene_name = get_one_text(soup=entrezgene, path="Gene-ref_locus")
            if not gene_name:
                gene_name = get_one_text(soup=entrezgene, path="Gene-ref_desc")
            # if we want RNA in the future
            # if not gene_name: 
            #     gene_name = get_one_text(soup=entrezgene, path="Gene-ref_locus-tag")

            accn = get_one_text(soup=entrezgene, path="Gene-commentary_accession")
            accn_version = get_one_text(soup=entrezgene, path="Gene-commentary_version")
            try:
                accn = accn + "." + accn_version
            except: # Gene exists but not sequencied yet
                continue

            complementar = parse_one_xml(soup=entrezgene, path="Na-strand")
            try:
                complementar = True if complementar["value"] == "minus" else False
            except TypeError:
                complementar = False

            # if returns None means the gene is the entire sequence
            try:
                gene_start = int(get_one_text(soup=entrezgene, path="Seq-interval_from"))
                gene_end = int(get_one_text(soup=entrezgene, path="Seq-interval_to"))
            except TypeError:
                gene_start = None
                gene_end = None
            genes_info[gene_name] = (accn, (gene_start, gene_end), complementar)
        
        specie_mod = '_'.join(specie.split(' '))
        write_pickle(genes_info, f"tmp_{specie_mod}/genes_info_{specie_mod}_{cont}.tmp")

        cont += 1
        retstart += retmax
        if count <= retstart:
            return


def gets_genes_seqs(esearch_api, efetch_api, esearch_parameters, efetch_parameters, specie, study_specie, genes_list=""):
    """Function that reads the gene_data from tmp_files, searchs that data on NCBI and writes to the respective files

    Args:
        esearch_api (str): esearch api of NCBI, in this case "esearch.fcgi"
        efetch_api (str): efetch api of NCBI, in this case "efetch.fcgi"
        esearch_parameters (dict): esearch parameters
        efetch_parameters (dict): efetch parameters
        specie (str): Name of the specie to search the data
        study_specie (str): Name of the study species dir
        genes_list (str, optional): List of the genes that was searched before on NCBI. Defaults to "".
    """
    # Getting the sequences of the accession numbers
    if "retmode" in efetch_parameters:
        del efetch_parameters["retmode"]
    if "retstart" in efetch_parameters:
        del efetch_parameters["retstart"] 
    if "retmax" in efetch_parameters:
        del efetch_parameters["retmax"] 
 
    esearch_parameters["db"] = "nucleotide"
    efetch_parameters["db"] = "nucleotide"
    efetch_parameters["rettype"] = "fasta"
    used_accns = []
    specie_mod = '_'.join(specie.split(' '))
    genes_info_files = listdir(f"./tmp_{specie_mod}/")

    for genes_info_file in genes_info_files:
        with open(f"./tmp_{specie_mod}/{genes_info_file}", "rb") as tmp:
            genes_info = pickle.load(tmp)
            genes_info = {gene: data for gene, data in sorted(genes_info.items(), key=lambda item: item[1])}
        
        genes_list = [gene.upper() for gene in genes_list]

        for gene, data in genes_info.items():
            if gene.upper() in genes_list or not genes_list:
                accn = data[0]
                esearch_parameters["term"] = accn
                if accn not in used_accns:
                    used_accns.append(accn)
                else:
                    continue
                efetch_parameters["query_key"], efetch_parameters["WebEnv"] = \
                get_querykey_webenv(esearch_api, esearch_parameters)
                genome = get_ncbi_data(efetch_api, efetch_parameters, wtaf=True, output_file=f"{study_specie}/genome/{specie}/{specie}_genome.fasta")
                write_pickle(genome, f"./tmp_{specie_mod}/{accn}_data.tmp")

        # Getting all the genes of the species
        accn_tmp = ""
        for gene, data in genes_info.items():
            if gene.upper() in genes_list or not genes_list:
                accn = data[0]
                start, end = data[1]
                complementar = data [2]
                if accn != accn_tmp:
                    with open(f"./tmp_{specie_mod}/{accn}_data.tmp", "rb") as tmp:
                        genome = pickle.load(tmp)
                    genome = "".join(genome.split("\n")[1:])
                accn_tmp = accn
                header = f">{accn} {specie}, {gene} gene\n"
                gene_sequence = get_gene_from_genome(genome, start, end + 1)
                if complementar:
                    gene_sequence = get_complement_gene_sequence(gene_sequence)
                gene_sequence = format_gene(gene_sequence)
                write_to_a_file(header + gene_sequence, f"{study_specie}/genes/{specie}/{gene}.fasta")


def get_info_seq_genes(esearch_api, efetch_api, esearch_parameters, efetch_parameters, specie, gene_names, study_specie):
    """Executes the get_genes_info and gets_genes_seqs functions

    Args:
        esearch_api (str): esearch api of NCBI, in this case "esearch.fcgi"
        efetch_api (str): efetch api of NCBI, in this case "efetch.fcgi"
        esearch_parameters (dict): esearch parameters
        efetch_parameters (dict): efetch parameters
        specie (str): Name of the specie to search the data
        study_specie (str): Name of the study species dir
        genes_list (str, optional): List of the genes that was searched before on NCBI
    """
    error = get_genes_info(esearch_api, efetch_api, esearch_parameters, efetch_parameters, specie, study_specie, gene_names)
    if error:
        return
    gets_genes_seqs(esearch_api, efetch_api, esearch_parameters, efetch_parameters, specie, study_specie, gene_names)

def main(species, level, api_key=""):
    """Main function

    Args:
        species (str): Species in study
        level (str): Level taxonomic in study
        api_key (str, optional): API key from NCBI if multiprocessing is wanted. Defaults to "".

    Yields:
        str: Name of the species that the program finds
    """
    # Creating some initial parameters
    specie = species # Name of the specie
    specie_mod = '_'.join(specie.split(' '))
    yield specie_mod
    rank = level # Taxonomic level
    esearch_api = "esearch.fcgi"
    efetch_api = "efetch.fcgi"
    esearch_parameters = {
        "db": "taxonomy",
        "term": specie if specie.lower().endswith("[organism]") else specie + "[organism]",
        "usehistory": "y",
        "retmode": "xml"
    }

    if api_key:
        esearch_parameters["api_key"] = api_key
    efetch_parameters = esearch_parameters.copy()
    del efetch_parameters["term"]
    # Getting the query_key and the WebEnv of taxonomy ncbi database for the specie
    efetch_parameters["query_key"], efetch_parameters["WebEnv"] = \
    get_querykey_webenv(esearch_api, esearch_parameters)

    # Getting the taxonomy data from ncbi for the specie
    efetch_specie = get_ncbi_data(efetch_api, efetch_parameters, wtaf=True, output_file=f"{specie_mod}/xml/{specie}.xml")

    # Getting the name of the taxonomy level (rank name) of the specie
    taxons = parse_all_xml(efetch_specie, "LineageEx Taxon")
    for taxon in taxons:
        if get_one_text(soup=taxon, path="Rank") == rank:
            rank_name = get_one_text(soup=taxon, path="ScientificName")
            esearch_parameters["term"] = rank_name + "[organism]"
            break

    # Getting the query_key and the WebEnv of taxonomy ncbi database for the rank of the specie
    efetch_parameters["query_key"], efetch_parameters["WebEnv"] = \
    get_querykey_webenv(esearch_api, esearch_parameters)
    # Getting the taxonomy data from ncbi for the rank of the specie
    efetch_rank = get_ncbi_data(efetch_api, efetch_parameters, wtaf=True, output_file=f"{specie_mod}/xml/{rank_name}.xml")

    # Getting the name of all species for that taxonomic level
    taxons = parse_all_xml(efetch_rank, "Taxon CreateDate") # Getting a child of the biggest taxons. There are
                                                            # smaller taxons but are too many and are useless.
    taxons = [taxon.parent for taxon in taxons] # Getting the biggest taxons (the parent of "CreateDate")
    species = [get_one_text(soup=taxon, path="ScientificName") for taxon in taxons if get_one_text(soup=taxon, path="Rank") == "species"]
    species.remove(specie)
    species_mods = ['_'.join(specie.split(' ')) for specie in species]
    for species_mod in species_mods:
        yield species_mod

    # Getting the gene names for the specie in study
    error = get_genes_info(esearch_api, efetch_api, esearch_parameters, efetch_parameters, specie, specie_mod)
    genes_info_files = listdir(f"./tmp_{specie_mod}/")
    gene_names = []

    for genes_info_file in genes_info_files:
        with open(f"./tmp_{specie_mod}/{genes_info_file}", "rb") as tmp:
            genes_info = pickle.load(tmp)
            for gene_name in genes_info.keys():
                gene_names.append(gene_name)
    genes_names = [gene_name.upper() for gene_name in gene_names]
    gets_genes_seqs(esearch_api, efetch_api, esearch_parameters, efetch_parameters, specie, specie_mod)
    
    if api_key:
        pool = Pool(5)
        # Getting genes info and store it in a temp file with pickle
        # The data is formated in {gene: (accn, (start, end), is_complementar)}
        pool.map(partial(get_info_seq_genes, esearch_api, efetch_api, esearch_parameters, efetch_parameters, study_specie=specie_mod, gene_names=gene_names), species)
    else:
        for specie in species:
            get_info_seq_genes(esearch_api, efetch_api, esearch_parameters, efetch_parameters, specie, gene_names, specie_mod)

if __name__ == '__main__':
    # Example of how to use:
    # ./ncbi_downloader.py "Passer domesticus" "family" "your_api_key"
    start = time()
    try:
        api_key = argv[3]
    except IndexError:
        api_key = ""

    try:
        species = []
        for specie in main(argv[1], argv[2], api_key):
            species.append(specie)
    except KeyboardInterrupt:
        print("\nExitting the application", file=stderr)
    except BaseException as e:
        print(traceback.format_exc(), file=stderr)
        print("\nExitting the application", file=stderr)
    for specie in species:
        if isdir(f"./tmp_{specie}"):
            rmtree(f"./tmp_{specie}")
    end = time()
    min = int((end-start)//60)
    min = "0" + str(min) if min < 10 else str(min)
    sec = int((end-start)%60)
    sec = "0" + str(sec) if sec < 10 else str(sec)
    print(min + ":" + sec + "min")
